# app_workstation

Basics stuff to install on a fresh workstation with Puppet Apply

## Ubuntu 18.04

For the very first run, as user **root**, do:

```shell
sudo apt install git
mkdir -p /root/.puppetlabs/etc/code/modules/
cd /root/.puppetlabs/etc/code/modules/
git clone https://gitlab.com/matt.faure/puppet_workstation.git
/root/.puppetlabs/etc/code/modules/puppet_workstation/files/puppet_workstation.sh
```

If this puppet code is modified, and we want to update a workstation:

1. Get to the parent directory of this repos
1. Then run 

```shell
sudo /opt/puppetlabs/bin/puppet apply --modulepath=/root/.puppetlabs/etc/code/modules/ -e 'include puppet_workstation'
```